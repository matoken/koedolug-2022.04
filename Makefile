SOURCE=slide.adoc
SLIDE=slide
BASE=$(basename ${SOURCE})
HTML=${BASE}.html
SINGLESLIDEHTML=${BASE}.SingleSlide.html
SINGLEHTML=${BASE}.Single.html
PDF=${BASE}.pdf

all: $(HTML) $(PDF)

html: $(SLIDE)/$(SOURCE)
	asciidoctor-revealjs-linux ${SLIDE}/${SOURCE}

pdf: ${SLIDE}/$(HTML)
	chromium --user-data-dir=$(mktemp -d) --headless --disable-gpu --print-to-pdf=${SLIDE}/${PDF} file://`pwd`/${SLIDE}/${HTML}?print-pdf
	gpg --yes --detach-sign ${SLIDE}/$(PDF)

singlefile: ${SLIDE}/${HTML}
	rm ${SLIDE}/${SINGLESLIDEHTML}
	docker run -v `pwd`/${SLIDE}:/usr/src/app/out singlefile file:///usr/src/app/out/${HTML} ${SINGLESLIDEHTML} --back-end=webdriver-chromium --remove-scripts=false
	rm ${SLIDE}/${SINGLEHTML}
	docker run -v `pwd`/${SLIDE}:/usr/src/app/out singlefile file:///usr/src/app/out/${HTML}?print-pdf ${SINGLEHTML} --back-end=webdriver-chromium
clean:
	rm -f $(PDF) $(PDF).sig $(HTML)

